<?php
require_once("vendor/autoload.php");
use Viber\Client;
$apiKey = '46f78102e667d036-afbfae9677ee3446-c5189821c565df25'; // <- PLACE-YOU-API-KEY-HERE
$webhookUrl = 'http://gifamin.com/bot.php'; // <- PLACE-YOU-HTTPS-URL
try {
    $client = new Client([ 'token' => $apiKey ]);
    $result = $client->setWebhook($webhookUrl);
    echo "Success!\n";
} catch (Exception $e) {
    echo "Error: ". $e->getError() ."\n";
}