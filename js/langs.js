var langs = ['ru', 'ua'];
var langCode = '';
var langJS = null;
var defaultlang = 'ru';


var translate = function (jsdata)
{	
	$("[tkey]").each (function (index)
	{
		var strTr = jsdata [$(this).attr ('tkey')];
	    $(this).html (strTr);
	});
	
	
	$("[tplaceholder]").each (function (index)
	{
		var strTr = jsdata [$(this).attr ('tplaceholder')];
	    $(this).attr ('placeholder',strTr);
	});
	
}




//$.removeCookie('mylang');


function superlang(){
	if($.cookie('mylang')){
		ml = $.parseJSON($.cookie("mylang"));
		storedlang = ml.lang;
}else{
	    var startlang = navigator.language.substr (0, 2);
	    
	    
	    if ( $.inArray( startlang, langs ) > -1 ){
		    $.cookie("mylang", '{"lang":"'+startlang+'"}');
		}else{
		    $.cookie("mylang", '{"lang":"'+defaultlang+'"}');
	    }
	    
	   ml = $.parseJSON($.cookie("mylang"));
	   storedlang = ml.lang;
}


translator = storedlang;
}



superlang();


function changeLang(l){
	translator = l;
	$.removeCookie('mylang');
	$.cookie("mylang", '{"lang":"'+l+'"}');
	   ml = $.parseJSON($.cookie("mylang"));
	   storedlang = ml.lang;
	   translator = storedlang;
	   $.getJSON('lang/'+translator+'.json', translate);
}

