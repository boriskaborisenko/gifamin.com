function initMap() {
	// 50.416030, 30.518440
	//50.417275, 30.516747
	//50.417240, 30.516695
	var a = 50.417240;
	var b = 30.516695;
	
	
    var myLatLng = {lat: a, lng: b};
    var mypin = '/images/svg/pin_blue.svg'; 

  var minZoomLevel = 3;
    var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 17,
    scrollwheel: false,
    streetViewControl: false,
    center: {
	  lat: a,
      lng: b
    }
  });
  
  // Limit the zoom level
   google.maps.event.addListener(map, 'zoom_changed', function() {
     if (map.getZoom() < minZoomLevel) map.setZoom(minZoomLevel);
   });
  
  
  

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    icon:mypin,
    size: new google.maps.Size(100, 100),     // original size you defined in the SVG file
    scaledSize: new google.maps.Size(60, 60), // desired display size
    animation: google.maps.Animation.DROP
  });



  var mapStyles = [
					{
						"featureType": "landscape",
						"stylers": [
							{ "visibility": "on" },
							{ "hue": "#00ccff" },
							{ "saturation": -96 },
							{ "lightness": 0 },
							{ "gamma": 1.03 }
						]
					},{
						"featureType": "water",
						"stylers": [
							{ "visibility": "on" }
						]
					},{
						"featureType": "water",
						"elementType": "labels",
						"stylers": [
							{ "visibility": "on" }
						]
					},{
						"featureType": "administrative",
						"stylers": [
							{ "visibility": "on" }
						]
					},{
						"featureType": "administrative",
						"elementType": "labels",
						"stylers": [
							{ "visibility": "on" }
						]
					},{
						"featureType": "poi",
						"stylers": [
							{ "visibility": "on" },
							{ "hue": "#ffe500" },
							{ "saturation": -98 },
							{ "lightness": 3 },
							{ "gamma": 1 }
						]
					},{
						"featureType": "road",
						"stylers": [
							{ "visibility": "on" }
						]
					},{
						"featureType": "transit",
						"stylers": [
							{ "visibility": "on" }
						]
					}
				];

				
				map.setOptions({styles: mapStyles});





}


