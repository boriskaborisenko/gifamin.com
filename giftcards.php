<?php
	$root = 'http://'.$_SERVER['SERVER_NAME'].'/';
	include('connect.php');
	?>
<!--32708461.9757ccd.2a3c9f27fa2d482dbd143e7c0049303f-->
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
			<title>GIFAMIN | GIFTCARDS</title>
			<?php include('php/static/head.php');?>
	
	<style>
		html, body{background: #fff;}
	</style>		
</head>
<body>



<script>
	if(checkdev == 'mob'){
		window.location.replace('http://m.gifamin.com/giftcards');
	}
</script>





<?php include('php/static/top.php');?>
	
	



<div class="giftcards_hello">
	<div class="giftcards_hello_inner">
		<div class="giftcards_hello_left inl_t"></div>
		<div class="giftcards_hello_right inl_t">
		 <div class="pre_right">	
			
					<div class="giftcards_title">Подарочный сертификат</div>
					<div class="giftcards_text">Идеальный способ поздравить человека и сохранить право выбора за ним</div>
			
		 </div>
		</div>
	</div>
</div>

<div class="giftcards_block">
	<div class="inner">
		
		<div class="giftcards_header">Номиналы</div>
		
		<div class="all_gifts">
			
			<div class="one_gift_card">
				<div class="giftcard_face gc_100"></div>
				<!-- <div class="giftcard_anno">anno text</div> -->
				<!-- <div class="giftcard_button">Купить</div> -->
			</div>
			
			<div class="one_gift_card">
				<div class="giftcard_face gc_200"></div>
				<!-- <div class="giftcard_anno">anno text</div> -->
				<!-- <div class="giftcard_button">Купить</div> -->
			</div>
			
			<div class="one_gift_card">
				<div class="giftcard_face gc_500"></div>
				<!-- <div class="giftcard_anno">anno text</div> -->
				<!-- <div class="giftcard_button">Купить</div> -->
			</div>
			
			<div class="one_gift_card">
				<div class="giftcard_face gc_1000"></div>
				<!-- <div class="giftcard_anno">anno text</div> -->
				<!-- <div class="giftcard_button">Купить</div> -->
			</div>
			
		</div>
	</div>
</div>

<div class="giftcards_block">
	<div class="inner g_small_anno">
		<div class="giftcards_rules_title">
			Правила использования подарочного сертификата:
		</div>
		<div class="giftcards_rules">
			<ul>
				<li>Подарочный сертификат (далее «сертификат») является сертификатом на предъявителя (не является именным), и любое физическое лицо, предъявившее сертификат, может приобрести с его помощью товар, предлагаемый компанией Gifamin. 
				</li>
				
				<li>
				В случае утраты сертификата, в том числе хищения, сертификат не может быть восстановлен, а денежные средства не могут быть возвращены, в связи с отсутствием персонификации лица, владевшего сертификатом до утраты.
				</li>
				
				<li>
				 При оплате заказа с помощью сертификата обязательно укажите это на этапе оформления и сообщайте менеджеру при подтверждении заказа. Сертификат должен быть предъявлен и передан сотруднику компании при оплате товара. 
				</li>
				
				<li>
				Срок действия Сертификата ограничен и составляет 6 месяцев с даты выдачи.
				</li>
				
				<li>
				С использованием сертификата возможна единственная покупка. При совершении покупки номинал сертификата списывается целиком. В случае, если стоимость покупки меньше номинала сертификата, разница между стоимостью не возвращается. 
				</li>
				
				<li>
				При возврате товара, купленного с использованием сертификата, возврат средств осуществляется на новый сертификат аналогичного номинала.
				</li>
				
				<li>
				В случае, если стоимость покупки превышает номинал, разница между стоимостью товара и номиналом сертификата оплачивается наличными денежными средствами.
				</li>
				
				<li>
				Сертификат не может быть использован для получения наличных денежных средств.
				</li>
				
				<li>
				Оплатить товары с помощью сертификата могут только физические лица. Юридические лица не могут частично или полностью оплатить приобретение товаров сертификатами.
				</li>
				
				<li>
				Покупателю следует сохранять чек на покупку сертификата для подтверждения номинала карты в случае повреждения или иных спорных ситуаций. 
				</li>
			</ul>
		</div>
	</div>
</div>


				
<div class="giftcards_block" style="background: #fff;">
	<div class="inner">
		<div class="giftcards_header">Купить сертификаты</div>
		<div class="cards_amount_anno">Выберите количество</div>
		<div class="buy_card_box">
			<!-- <div class="buy_card_part">
				<div class="card_preview">Preview</div>
			</div> -->
			
			<div class="buy_card_part">
				<div class="card_vars">
					
					
					<?php
						$cards = array(100,200,500,1000);
						foreach($cards as $card){
							?>
							<div class="card_var inl_t">
								<div id="id_nom_<?php echo $card?>" class="card_nom inl_m"><?php echo $card?></div>
									<div class="card_amount inl_m">
										<div class="selbox_2">
										<select id="nom_<?php echo $card?>" name="sel_qty" class="amnt" onchange="card_amount(this.id);">
							 <?php
								 for($i=0;$i<11;$i++){
									 echo '<option value="'.$i.'">'.$i.' шт.</option>';
								 }
							 ?>				
							 			</select>
							 			</div>
							 	</div>
							 </div>
						<?php	
						}
					?>
					
					
					
					
					
				</div>
				
				
				<div class="cards_total">Общая стоимость: <span id="total_cards_sum">0 грн</span></div>
				<div class="cards_total_counter">Количество сертификатов: <span id="total_cards">0</span> шт</div>
				
				<div class="buy_cards_data">
					<div class="hover_buy_thanks off">
						<div class="card_thanx">Спасибо.</div>
						<div class="card_thanx_anno">Мы свяжемся с вами в ближайшее время</div>
					</div>
					<div class="hover_buy"></div>
					<div class="cards_buy_form">
						
						<div class="one_inp"><input class="inp chkt" type="text" value="" placeholder="Ваше имя" tplaceholder="your_name" id="un" onkeyup="userkeys(this.value, this.id)"></div>
							
							<div class="one_inp"><input class="inp chkt" type="text" value="" placeholder="+38 (___) ___-__-__" id="up" onkeyup="userkeys(this.value, this.id)"></div>
							<div class="one_inp"><input class="inp chkt" type="text" value="" placeholder="e-mail"  id="um" onkeyup="userkeys(this.value, this.id)"></div>
							<div class="one_inp"><input class="inp chkt" type="text" value="" placeholder="Куда доставить и комментарий" tplaceholder="del_addr" id="us" onkeyup="userkeys(this.value, this.id)"></div>
							
							
							<div class="note_card">Все поля обязательны для заполнения</div>
							
							<div class="final_btn">
								<div class="hider" id="final_step"></div>
							    <div class="btn2" onclick="buycards()">Купить</div>
							</div>
					
					</div>
				</div>
				
			</div>
			
			<!-- -->
		</div>
	</div>
</div>


<script>
	
	total_cards = 0;
	total_card_sum = 0;
	function card_amount(x){
		var card_100 = eval($('#nom_100').val());
		var card_200 = eval($('#nom_200').val());
		var card_500 = eval($('#nom_500').val());
		var card_1000 = eval($('#nom_1000').val());
		total_cards = card_100+card_200+card_500+card_1000;
		total_cards_sum = card_100*100+card_200*200+card_500*500+card_1000*1000;
		$('#total_cards').html(total_cards);
		$('#total_cards_sum').html(total_cards_sum+' грн');
		
		if(eval($('#'+x).val())>0){
			$("#id_"+x).addClass('colored_card');
		}else{
			$("#id_"+x).removeClass('colored_card');
		}
		
		if(total_cards > 0){
			$('.hover_buy').addClass('off');
		}else{
			$('.hover_buy').removeClass('off');
		}
		
	}
	
	
	
	function buycards(){
	
	var name = $('#un').val();
	var phone = $('#up').val();
	var mail = $('#um').val();
	var ship = $('#us').val();
	var c_100 = eval($('#nom_100').val());
	var c_200 = eval($('#nom_200').val());
	var c_500 = eval($('#nom_500').val());
	var c_1000 = eval($('#nom_1000').val());
	
	
	var data = '{"order":{"client":{';
	data = data+'"user":"'+name+'","phone":"'+phone+'","mail":"'+mail+'","shipping":"'+ship+'","total":"'+total_cards_sum+'","cards":"'+total_cards+'"';
	data = data+'}, "orderlist":{';
	data = data+'"100":'+c_100+',"200":'+c_200+',"500":'+c_500+',"1000":'+c_1000+'';
	data = data+'}}}';
	
	console.log(data);	
	 var xhttp = new XMLHttpRequest();
	 xhttp.open("GET", "php/cards_order.php?data="+data, true);
     xhttp.send();
	 
	 xhttp.onreadystatechange = function() {
	 if (this.readyState == 4 && this.status == 200) {
	    console.log(this.responseText);	
	    
	    $('.hover_buy_thanks').fadeIn(1000);
	    setTimeout(function(){
		    $('.hover_buy').removeClass('off');
		    $('#nom_100').val(0);
		    $('#nom_200').val(0);
		    $('#nom_500').val(0);
		    $('#nom_1000').val(0);
		    total_cards = 0;
			total_card_sum = 0;
		    $('.card_nom').removeClass('colored_card');
		    $('#total_cards').html(0);
			$('#total_cards_sum').html(0+' грн');
		}, 0);
		
		setTimeout(function(){
			$('.hover_buy_thanks').fadeOut(500);
		}, 3500);
	    
		
		
		
		}
      }
		
	};
	
	

	
	$('.all_top').css({'box-shadow':'none', 'border':'none', 'background':'transparent', 'position':'absolute'});
	$('.cartfull').css({'border':'none'});
	$.getJSON('lang/'+translator+'.json', translate);
</script>

<script src="js/main.js"></script>
</body>
</html>