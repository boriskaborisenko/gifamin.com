<?php

include('../php/origin/connect.php');	
	
 //include_once '../vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php';

function getXLS($xls, $sheet){
    include_once '../vendor/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php';
    $objPHPExcel = PHPExcel_IOFactory::load($xls);
    $objPHPExcel->setActiveSheetIndex($sheet);
    $aSheet = $objPHPExcel->getActiveSheet();
 
    //этот массив будет содержать массивы содержащие в себе значения ячеек каждой строки
    $array = array();
    //получим итератор строки и пройдемся по нему циклом
    
    
    foreach($aSheet->getRowIterator() as $row){
      //получим итератор ячеек текущей строки
      $cellIterator = $row->getCellIterator();
      
      //пройдемся циклом по ячейкам строки
      //этот массив будет содержать значения каждой отдельной строки
      $item = array();
      $cellIterator->setIterateOnlyExistingCells(false);
      foreach($cellIterator as $cell){
        //заносим значения ячеек одной строки в отдельный массив
        //array_push($item, iconv('utf-8', 'cp1251', $cell->getCalculatedValue()));
        array_push($item, $cell->getCalculatedValue());
      }
      //заносим массив со значениями ячеек отдельной строки в "общий массв строк"
      array_push($array, $item);
    }
    
    
    
    
    return $array;
  }
 
  
  
  
  
  
  
  
  function parseProducts($doc, $x){
	 global $conn;
	 $xlsData = getXLS($doc, $x); //извлеаем данные из XLS
	  foreach(array_slice($xlsData, 1) as $one){
	 //foreach($xlsData as $one){ 
		 if(!empty($one[0])){
		   
		   
		   
		   //echo $one[0].' | '.$one[1].' | '.$one[2].' | '.$one[3].'<br>';
		   $id = $one[0];
		   $cat_id = $one[1];
		   $prov_id = $one[2];
		   $name = $one[3];
		   $desc = $one[4];
		   $imgs = $one[5];
		   $he = $one[6];
		   $she = $one[7];
		   $kids = $one[8];
		   $home = $one[9];
		   $pets = $one[10];
		   $rec = $one[11];
		   $brand = $one[12];
		   //$prov_sku = $one[13];
		   
		   echo $name.'<br>';
		   
		   $query = $conn->query("SELECT * FROM products WHERE id='$id'");
		   if($query->num_rows > 0){
			   //echo 'exist';
			   $conn->query("UPDATE products SET id='$id', product_name='$name', provisioner_id='$prov_id', cat_id='$cat_id', description='$desc', img_array='$imgs', he='$he', she='$she', kids='$kids', house='$home', pets='$pets', recommend='$rec', brand_name='$brand' WHERE id='$id'");
		   }else{
			   //echo 'add';
			   $conn->query("INSERT INTO products (id, product_name, provisioner_id, cat_id, description, img_array, he, she, kids, house, pets, recommend, brand_name) VALUES ('$id', '$name', '$prov_id', '$cat_id', '$desc', '$imgs', '$he', '$she', '$kids', '$home', '$pets', '$rec', '$brand')");
		   }
       
    
		//echo '<br>';   
		   	 
		  
		  
		 
		 
		 } 
	  }
  }
  
  
  
  
  function parseVars($doc, $x){
	 global $conn;
	 $xlsData = getXLS($doc, $x); //извлеаем данные из XLS
	  foreach(array_slice($xlsData, 1) as $one){
	 //foreach($xlsData as $one){ 
		 if(!empty($one[0])){
		   
		   
		   
		   //echo $one[0].' | '.$one[1].' | '.$one[2].' | '.$one[3].'<br>';
		   $id = $one[0];
		   $prod_id = $one[1];
		   $var_name = $one[2];
		   $var_ext = $one[3];
		   $stock = $one[4];
		   $price_in = $one[5];
		   $price_out = $one[6];
		   $price_sale = $one[7];
		   $brand_sku = $one[8];
		   
		   echo $var_name.'<br>';
		   
		   $query = $conn->query("SELECT * FROM variants WHERE id='$id'");
		   if($query->num_rows > 0){
			   //echo 'exist';
			   $conn->query("UPDATE variants SET id='$id', product_id='$prod_id', var_name='$var_name', var_name_extend='$var_ext', stock='$stock', price_in='$price_in', price_out='$price_out', price_out_sale='$price_sale' brand_sku='$brand_sku'  WHERE id='$id'");
		   }else{
			   //echo 'add';
			   $conn->query("INSERT INTO variants (id, product_id, var_name, var_name_extend, stock, price_in, price_out, price_out_sale, brand_sku) VALUES ('$id', '$prod_id', '$var_name', '$var_ext', '$stock', '$price_in', '$price_out', '$price_sale', '$brand_sku')");
		   }
       
    
		//echo '<br>';   
		   	 
		  
		  
		 
		 
		 } 
	  }
  }
  
  
  
  
  
  function parseCats($doc, $x){
	 global $conn;
	 $xlsData = getXLS($doc, $x); //извлеаем данные из XLS
	  foreach(array_slice($xlsData, 1) as $one){
	 //foreach($xlsData as $one){ 
		 if(!empty($one[0])){
		   
		   
		   
		   //echo $one[0].' | '.$one[1].' | '.$one[2].' | '.$one[3].'<br>';
		   $id = $one[0];
		   $cat_name = $one[1];
		  
		   
		   echo $cat_name.'<br>';
		   
		   $query = $conn->query("SELECT * FROM category WHERE id='$id'");
		   if($query->num_rows > 0){
			   //echo 'exist';
			   $conn->query("UPDATE category SET id='$id', cat_name='$cat_name', status=1  WHERE id='$id'");
		   }else{
			   //echo 'add';
			   $conn->query("INSERT INTO category (id, cat_name, status) VALUES ('$id', '$cat_name', 1)");
		   }
       
    
		//echo '<br>';   
		   	 
		  
		  
		 
		 
		 } 
	  }
  }    
      
      
  
  
  
  
  function parseProvs($doc, $x){
	 global $conn;
	 $xlsData = getXLS($doc, $x); //извлеаем данные из XLS
	  foreach(array_slice($xlsData, 1) as $one){
	 //foreach($xlsData as $one){ 
		 if(!empty($one[0])){
		   
		   
		   
		   //echo $one[0].' | '.$one[1].' | '.$one[2].' | '.$one[3].'<br>';
		   $id = $one[0];
		   $prov_name = $one[1];
		   $prov_city = $one[2];
		   $prov_face = $one[3];
		   $prov_phone = $one[4];
		   $prov_brand = $one[5];
		  
		   
		   echo $prov_face.'<br>';
		   
		   $query = $conn->query("SELECT * FROM provisioners WHERE id='$id'");
		   if($query->num_rows > 0){
			   //echo 'exist';
			   $conn->query("UPDATE provisioners SET id='$id', provisioner_name='$prov_name', city='$prov_city', contact_face='$prov_face', contact_details='$prov_phone', provisioner_brand='$prov_brand'  WHERE id='$id'");
		   }else{
			   //echo 'add';
			   $conn->query("INSERT INTO provisioners (id, provisioner_name, city, contact_face, contact_details, provisioner_brand) VALUES ('$id', '$prov_name', '$prov_city', '$prov_face', '$prov_phone', '$prov_brand')");
		   }
       
    
		//echo '<br>';   
		   	 
		  
		  
		 
		 
		 } 
	  }
  }      
      
      
      
      /*
	  products = 0;
	  variants = 1;
	  provisioners = 2;
	  cats = 3;
	  */
  
  
  
  
  
  $input_doc = '../uplds/'.$big_name;
  
  parseProducts($input_doc, 0);
  parseVars($input_doc, 1);
  parseProvs($input_doc, 2);
  parseCats($input_doc, 3);
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  