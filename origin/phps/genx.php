<?php
	
	
function rus2translit($string) {
    $converter = array(
        'а' => 'a',   'б' => 'b',   'в' => 'v',
        'г' => 'g',   'д' => 'd',   'е' => 'e',
        'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
        'и' => 'i',   'й' => 'y',   'к' => 'k',
        'л' => 'l',   'м' => 'm',   'н' => 'n',
        'о' => 'o',   'п' => 'p',   'р' => 'r',
        'с' => 's',   'т' => 't',   'у' => 'u',
        'ф' => 'f',   'х' => 'h',   'ц' => 'c',
        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
        'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
        'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
        
        'А' => 'A',   'Б' => 'B',   'В' => 'V',
        'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
        'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
        'И' => 'I',   'Й' => 'Y',   'К' => 'K',
        'Л' => 'L',   'М' => 'M',   'Н' => 'N',
        'О' => 'O',   'П' => 'P',   'Р' => 'R',
        'С' => 'S',   'Т' => 'T',   'У' => 'U',
        'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
        'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
        'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
        'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
    );
    return strtr($string, $converter);
}
function str2url($str) {
    // переводим в транслит
    $str = rus2translit($str);
    // в нижний регистр
    $str = strtolower($str);
    // заменям все ненужное нам на "-"
    $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
    // удаляем начальные и конечные '-'
    $str = trim($str, "-");
    return $str;
}
//echo transliterate("У попа была собака, он ее любил.");	
	
include('../includes/connect.php');
require "../../vendor/autoload.php";

use Dompdf\Dompdf;
$dompdf = new DOMPDF();



$datas = $_GET['data'];
//$data = str_replace('&nbsp;',' ', $data);
$obj = json_decode($datas,true);
//$test = json_encode($obj);

//print_r($obj);



$title = $obj['title'];
$text = $obj['text'];
$text = str_replace('*XXXXX*', "<br>", $text);
//$text = preg_replace("/*XXXXX*/is", "<br />", $text);
$author = $obj['author'];




if($author != 0){
	
	$query = $conn->query("SELECT * FROM authors WHERE id='$author'");
		while($row = $query->fetch_assoc()){
			$s = $row['signature'];
			$t = $row['rec_text'];
		
	
	$signature = '
	<div class="signature_block">
  <div class="signature_text">'.$t.'</div>
  <div class="signature_image"><img width="100" src="../../images/signs/black/'.$s.'"></div>
  </div>
	';	
	}
	
	


}else{
	$signature = '';
}


$data = '<html>
<style>

@font-face {
	font-family: "PB";
	src: url("../fonts/Proxima Nova Bold.eot");
	src: local("☺"), url("../fonts/Proxima Nova Bold.woff") format("woff"), url("../fonts/Proxima Nova Bold.ttf") format("truetype"), url("../fonts/Proxima Nova Bold.svg") format("svg");
	font-weight: normal;
	font-style: normal;
}

@font-face {
	font-family: "PL";
	src: url("../fonts/Proxima Nova Light.eot");
	src: local("☺"), url("../fonts/Proxima Nova Light.woff") format("woff"), url("../fonts/Proxima Nova Light.ttf") format("truetype"), url("../fonts/Proxima Nova Light.svg") format("svg");
	font-weight: normal;
	font-style: normal;
}

@font-face {
	font-family: "PR";
	src: url("../fonts/Proxima Nova Regular.eot");
	src: local("☺"), url("../fonts/Proxima Nova Regular.woff") format("woff"), url("../fonts/Proxima Nova Regular.ttf") format("truetype"), url("../fonts/Proxima Nova Regular.svg") format("svg");
	font-weight: normal;
	font-style: normal;
}

.header{font-size:20pt; font-family:"PB"; margin-bottom:50px;}
.text{font-size:12pt; line-height:13pt; font-family:"PL"; margin-top:20px;}
p{margin:0; padding:0}
.signature_text{font-family:"PR"; font-size:8pt; width:60%; display:inline-block; vertical-align:middle;}
.signature_image{font-family:"PR"; display:inline-block; vertical-align:middle; width:100px; margin-left:80px;}
.signature_block{margin-top:100px;}
.footer_logo{width:80px; height:auto; position:absolute; bottom:0px; left:50%; margin-left:-50px;}
</style>

 <body style="width: 100%; height: 100%; position:relative;">
  <div style="width:595px; margin:0 auto; position:relative;">
  
  <div style="margin:0 auto; width:85%;">
  
  <div class="header">'.$title.'</div>
  <div class="text">'.$text.'</div>
  
  
  
  
  '.$signature.'
  
  
 
  
  </div>
  
   <div class="footer_logo"><img width="80" src="../../images/footer_legend.png"></div>
  
  </div>
 </body>
</html>';


$html = "<<<'ENDHTML'";

$html .= $data;

$html .= "ENDHTML";

$dompdf->load_html($html);
$dompdf->render();


$filename = str2url($title);
$date = date("__d.m.Y");
$dompdf->stream($filename.''.$date.".pdf");

?>