function admTab(id, x){
	
	console.log(x);
	
	$('.adm_nav').removeClass('active_adm_nav');
	$('#'+id).addClass('active_adm_nav');
	
	var xhttp = new XMLHttpRequest();
	xhttp.open("GET", "pages/"+x+".php", true);
	xhttp.send();
	xhttp.onreadystatechange = function() {
    	if (this.readyState == 4 && this.status == 200) {
	    document.getElementById("adm_viewport").innerHTML = xhttp.responseText;
	    
	    

        if(x == 'upload'){
	        console.log('upload');
	             $('button:submit').attr('disabled',true);
            $('input:file').change(
                function(){
                    if ($(this).val()){
                        $('button:submit').removeAttr('disabled'); 
                    }
                    else {
                        $('button:submit').attr('disabled',true);
                    }
                });
	           
	        
	             var register = document.getElementById('uploadxls');

				 register.onsubmit = function(e){
				 e.preventDefault(); //it is working
				 $.ajax({
				 	url: "pages/upld.php",    // Url to which the request is send
				 	type: "POST",             // Type of request to be send, called as method
				 	data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				 	contentType: false,       // The content type used when sending data to the server.
				 	cache: false,             // To unable request pages to be cached
				 	processData:false,        // To send DOMDocument or non processed data file it is set to false
				 	success: function(data)   // A function to be called if request succeeds
				 	{
				 	$('#loading').hide();
				 	$("#adm_pop").removeClass('off');
				 	updatebase(data);
				 	}
				 });
				 
				 
				 }  
        }
        
        if(x == 'giftcards'){
	        console.log('giftcards');
        }

        if(x == 'legends'){
		    initSummernote();
	    }
 
		}
    };
}



function updatebase(x){
		var xhttp = new XMLHttpRequest();
		
		xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
		 //document.getElementById("adm_pop").innerHTML = this.responseText;
		 setTimeout(function(){
			$("#adm_pop").addClass('off');
			$('input:file').val(''); 
		 }, 2000);
		 
    	}
		};
		xhttp.open("GET", "../php/excel.php?iamfile="+x, true);
		xhttp.send();
		
	}




function supToggle(x){
		
		$("div[id^='s_row_']").removeClass('shad');
		$("div[id^='sd_']").addClass('hide');
		
		var ident_sup = x.substring(6);
		$('#s_row_'+ident_sup).toggleClass('shad');
		$('#sd_'+ident_sup).toggleClass('hide');
		
		//$('#s_'+ident_sup).html($('#s_'+ident_sup).text() == 'Свернуть' ? 'Детали' : 'Свернуть');
	};


//LEGENDS START
function initSummernote(){
		    $('#summernote').summernote({
	  //fontNames: ['PB', 'PR', 'PL'],
	  placeholder: 'write here...',
	  toolbar: [
		  
	  ['view', ['codeview']]
	/*
	['style', ['style']],
    ['font', ['bold', 'italic', 'underline', 'clear']],
    ['fontname', ['fontname']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height']],
    ['table', ['table']],
    ['insert', ['link', 'picture', 'hr']],
    ['view', ['fullscreen', 'codeview']],
    ['help', ['help']]  
	 */ 
    
  ]
  
  
  });

	    }
	    
	    
function gen(){
	title = $('#title').val();
	text =  $('.note-editable').html();
	text = text.replace(/<\/p>+/g,"");
	text = text.replace(/<p>+/g,"");
	text = text.replace(/&nbsp;+/g,"");
	text = text.replace(/<br>+/g,"*XXXXX*");
	text = text.replace(/<br \/>+/g,"*XXXXX*");
	//text = $('#buffer').val();
	//text = text.replace('&nbsp;', '');
	//newtext = $('#output').html(text);
	//newtext2 = document.getElementById('output').innerHTML;
	
	
	author = $('#author').val();
	//console.log(newtext2);
	datas = '{"title":"'+title+'","text":"'+text+'","author":"'+author+'"}';
	//datas = JSON.stringify(datas);
	//console.log(datas);
	url = 'phps/genx.php?data='+datas;
	//url = 'g.php?data='+datas;
	
	/*
	var xhttp = new XMLHttpRequest();
		xhttp.open("GET",  "gen_mix.php?data="+datas, true);
		
		xhttp.send();
		xhttp.onreadystatechange = function() {
    		if (this.readyState == 4 && this.status == 200) {
				//resp = xhttp.responseText;
				//location.href='/php/gentemp.php';
				}
		};
	*/
	
	//alert(datas);
	location.href = url;
	console.log(datas);
}


function byid(){
	console.log('BY ID');
	id = $('#byid').val();
	if(id.length>0){
	   
	   var xhttp = new XMLHttpRequest();
		xhttp.open("GET",  "phps/genid.php?id="+id, true);
		
		xhttp.send();
		xhttp.onreadystatechange = function() {
    		if (this.readyState == 4 && this.status == 200) {
				resp = JSON.parse(xhttp.responseText);
				//json = JSON.parse(resp);
				title_x = resp['title'];
				gettext(id);
				author_x = resp['author'];
				$('#title').val(title_x);
				if(author_x!=0){
				 $('#author').val(author_x);	
				}else{
				 $('#author').val(0);	
				}
				
				console.log(title_x);
				
				}
		};
	   	
	}else{
		$('#title').val('');
		$('#author').val(0);
		
		$('#summernote').summernote('destroy');
				$('#summernote').summernote({
					placeholder: 'write here...',
					toolbar: [
					['view', ['codeview']]
					]
				});
				
				
		console.log('wrong id');
	}
	
}

function gettext(id){
	var xhttp = new XMLHttpRequest();
		xhttp.open("GET",  "phps/genid_text.php?id="+id, true);
		
		xhttp.send();
		xhttp.onreadystatechange = function() {
    		if (this.readyState == 4 && this.status == 200) {
				resp = xhttp.responseText;
				
				//regresp = resp.replace(/<br\s*[\/]?>/gi, "\n");
				$('#summernote').summernote('destroy');
				$('#summernote').summernote({
					placeholder: 'write here...',
					toolbar: [
					['view', ['codeview']]
					]
				});
				
				$('#summernote').summernote('code', resp);
				
					
				//resp = resp.replace('<br />', 'XXXXXXXXX');				
				//$('#buffer').val(resp);
				console.log(resp);
				
				}
		};
}
//LEGENDS END