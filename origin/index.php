<?php
/**
 * Copyright (C) 2013 peredur.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';

sec_session_start();

if (login_check($mysqli) == true) {
    $logged = 'in';
    header("Location: /origin/control");
} else {
    $logged = 'out';
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Secure Login: Log In</title>
        <link rel="stylesheet" href="../css/main.min.css" />
        <script type="text/JavaScript" src="js/sha512.js"></script> 
        <script type="text/JavaScript" src="js/forms.js"></script>
       
    </head>
    <body class="adm_body">
        <?php
        if (isset($_GET['error'])) {
            echo '<p class="error">Error Logging In!</p>';
        }
        ?> 
        
        <div class="entrance">
        
         <div class="center_box">
	         
	         <form action="includes/process_login.php" method="post" name="login_form"> 
		     
		     <div class="adm_icon"></div>    			
            <div><input class="inp_adm inl_m" type="text" name="email" placeholder="email"/></div>
            <div><input class="inp_adm inl_m" type="password" 
                             name="password" 
                             id="password"
                             placeholder="password"/></div>
            <div class="over_b"><button class="adm_auth" onclick="formhash(this.form, this.form.password);"/>Authorize</button></div>
        </form>
	         
         </div>
        
        
        </div>
        
       <!--  <p>If you don't have a login, please <a href="register.php">register</a></p>
        <p>If you are done, please <a href="includes/logout.php">log out</a>.</p>
        <p>You are currently logged <?php echo $logged ?>.</p> -->
        
    </body>
    
    

    
    
</html>
