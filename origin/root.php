<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
sec_session_start();
if (login_check($mysqli) == true) : ?>
<?php include('static/adm_top.php');?>
     
     
     
     
     
     
     <div class="adm_cont">
	     <div class="adm_inner">
	        
	        
	        <div id="adm_pop" class="adm_pop off">
		      <div id="inpop" class="adm_inpop">
			      <div class="adm_inpop_window">
				      <div class="pbar">
					      <div  class="loader"></div>
					  </div> 
				      <div class="ptext">Wait</div>
			      </div>
		      </div>    
		    </div>
	        
	        
	        <div id="adm_viewport">
		      <?php include('pages/dashboard.php');?>
	        </div>
	        
	     
	        
	        
     	     
	     </div>
	 </div>
     
     
       
        



 <script src="js/adm.js"></script>


<?php include('static/adm_bottom.php');?>
<?php else : ?>
            root not logined
            <a href="./index.php">login</a>
<?php endif; ?>