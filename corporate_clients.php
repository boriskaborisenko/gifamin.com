<?php
	$root = 'http://'.$_SERVER['SERVER_NAME'].'/';
	include('connect.php');
	?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
			<title>GIFAMIN | Corporate clients</title>
			<?php include('php/static/head.php');?>
	
	<style>
		html, body{background: #fff;}
	</style>		
</head>
<body style="overflow-x: hidden;">



<script>
	if(checkdev == 'mob'){
		window.location.replace('http://m.gifamin.com/clients');
	}
</script>





<?php include('php/static/top.php');?>
	
	

<?php
	$mo = date("M");
	$da = date("j");
$translate = array(
 "Jan:Январь",
 "Feb:Февраль",
 "Mar:Март",
 "Apr:Апрель",
 "May:Май",
 "Jun:Июнь",
 "Jul:Июль",
 "Aug:Август",
 "Sep:Сентябрь",
 "Oct:Октябрь",
 "Nov:Ноябрь",
 "Dec:Декабрь",
	);
	
	foreach($translate as $t){
		 $m = explode(':',$t);
		 if($m[0] == $mo){
			 $mx = $m[1];
		 }
	}
	
	
?>


<div class="clients_banner">Корпоративным клиентам</div>


<div class="agreement" style="width: 650px!important; margin: 0 auto; padding: 10px 0 100px 0;">
<!-- <h2 style="text-transform: uppercase; margin-bottom: 0px;">Корпоративным клиентам</h2> -->



<!-- <p>                                  
У нас особенный подход к каждой компании. Что мы предлагаем корпоративным клиентам?
</p> -->

<h3 style="margin-bottom: 0px;  font-family: 'PB'; font-weight: 300; font-size: 22px;">Индивидуальные наборы</h3>
<p class="corp_text">
Кроме наборов, представленных в каталоге, есть возможность собрать особенные наборы по вашим пожеланиям. Мы найдем именно тот алкогольный напиток, который хочет видеть к празднику наш клиент, и подберем грамотное сочетание к нему. 
</p>

<h3 style="margin-bottom: 0px; font-family: 'PB'; font-weight:300; font-size: 22px;">Брендирование</h3>
<p class="corp_text">
Размещение логотипа вашей компании на подарочном наборе. Брендирование — это отличный вариант создать имидж компании, обзавестись новыми клиентами и партнерами или поздравить существующих.
</p>

<h3 style="margin-bottom: 0px; font-family: 'PB'; font-weight:300; font-size: 22px;">Индивидуальное поздравление</h3>
<p class="corp_text">
Каждый подарок удивит вас «легендой» – творческим описанием набора. Мы с удовольствием напишем персональное поздравление, обращаясь к получателю по имени, и с пожеланиями, которые вы сообщите нам заранее.
</p>





 <div style="font-family: 'PR'; padding-top: 40px; margin-top: 40px; border-top: 1px solid rgba(0,0,0,0.1); font-size: 0;"> 
	 <div style="display:inline-block; vertical-align: middle; font-size: 10px; width: 480px;box-sizing: border-box; padding-right: 50px;">
		 <div style="font-size: 20px; font-family: 'PB';">Подарочная коробка</div>
		 <p class="corp_text_2">Не знаете во что упаковать ваш подарок? Мы об этом уже позаботились...</p>
		 
		 
		 
	 </div>
	 <div style="display:inline-block; vertical-align: middle; font-size: 10px; width: 160px; height: 70px;">
		 
		 <p class="corp_text_2" style="margin-top: 30px;"><a href="/box" target="_blank" style="border:1px solid #229fff; border-radius: 4px; text-decoration: none; padding: 10px 20px; color:#fff; font-family: 'PB'; background: #229fff">Подробнее</a></p>
	 </div>
 </div>  



<hr style="border: 0;
    height: 0;
    border-top: 1px solid rgba(0, 0, 0, 0.1);
    border-bottom: 1px solid rgba(255, 255, 255, 0.3); margin: 40px 0;">

<div class="calendar inl_t" style="width: 120px; height: 120px; background: url('../images/calendar3.png') center center no-repeat; background-size: cover;">
	<div class="cal_month" style="text-align: center; background: rgba(255,0,255,0.0); font-weight: bold; text-transform: uppercase; margin-top: 10px;  font-size: 11px; color:#fff;"><?php echo $mx?></div>
	<div class="cal_date" style="text-align: center; font-size: 60px; font-weight: bold; margin-top: 35px;"><?php echo $da?></div>
</div>





<div class="calendar_text inl_t" style="width:500px; margin-left: 20px;">
	<span style="margin-top: 0!important; font-family: 'PB'; font-weight:300; font-size: 20px;">Календарь поздравлений партнеров</span>

<p class="corp_text_2" style="margin-top: 7px;">Грамотное решение для всех, кто регулярно ломает голову вопросом, что подарить партнеру и как сделать это вовремя. 
Вы предоставляете необходимую информацию об именинниках (на 3, 6, 9 или 12 месяцев), а дальнейшие хлопоты мы берем на себя. Далее мы информируем вас о проделанной работе.</p>
</div>








<hr style="border: 0;
    height: 0;
    border-top: 1px solid rgba(0, 0, 0, 0.1);
    border-bottom: 1px solid rgba(255, 255, 255, 0.3); margin: 40px 0;">
    
    
 

<p class="corp_text">
Подарок со вкусом, в презентабельной упаковке, подобранный специально под клиента, становится особенным и повышает авторитет компании в глазах партнеров! 
</p>

<div class="corp_contacts">    
<span style="font-family: 'PB'; font-size: 20px;">Контакты:</span>
<p class="corp_text_3" style="margin-top: 8px; margin-bottom: 0;">
	<span>tel: +38 (093) 221-28-08</span><br>
	email: info@gifamin.com
</p>
</div>





<p style="font-family: 'PR'; text-align: left;">
<span style="font-family: 'PB'; margin-top: 20px;">Товариство з обмеженою відповідальністю «ГІФАМІН»</span><br>
Юридична адреса: 03028, м. Київ, Голосіївський Район,<br>
вулиця Добрий  Шлях, будинок  5, квартира 11.<br>
Банківські реквізити:п/р №26000053161475, <br>
в ПАТ «КБ «Приватбанк», МФО банку 321842<br>
Директор: Михайленко О. Г.<br>
Код ЄДРПОУ 40659768
</p>



</div>




<script>
	$('.all_top').css({'box-shadow':'none', 'border':'1px solid #eeeeee', 'background':'#FAFBFC', 'position':'absolute'});
	$('.cartfull').css({'border':'none'});
	 $.getJSON('lang/'+translator+'.json', translate);
</script>

<script src="<?php echo $root;?>/js/main.js"></script>
</body>
</html>