<?php

include('connect.php');



$item_per_page = 10;

$page_number = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);

if(!is_numeric($page_number)){
    header('HTTP/1.1 500 Invalid page number!');
    exit();
}

$position = (($page_number-1) * $item_per_page);


$tag = $_POST['tag'];

/* 
if($tag == "all"){
	$results = $conn->prepare("SELECT
	id, product_name, cat_id, price, stock, image, img_array, description
	FROM products ORDER BY id DESC LIMIT ?, ?");	
	$results->bind_param("dd",  $position, $item_per_page); 
} 
*/


if(is_numeric($tag)){
	$results = $conn->prepare("SELECT 
    products.id, products.product_name, products.cat_id, products.img_array, products.description,
		variants.product_id,
		variants.price_out,
		variants.price_out_sale
		FROM products
		LEFT JOIN variants
		ON products.id = variants.product_id
    
   WHERE cat_id=? 
   GROUP BY products.id
   ORDER BY id DESC LIMIT ?, ?");	
    $results->bind_param("ddd", $tag, $position, $item_per_page);
}

if(!is_numeric($tag)){
	if($tag == "all"){
		/*
		$results = $conn->prepare("SELECT 
		id, product_name, cat_id, price, stock, image, img_array, description
		FROM products ORDER BY id DESC LIMIT ?, ?");	
		$results->bind_param("dd",  $position, $item_per_page); 
		*/
		
		
		$results = $conn->prepare("SELECT 
		products.id, products.product_name, products.cat_id, products.img_array, products.description,
		variants.product_id,
		variants.price_out,
		variants.price_out_sale
		FROM products
		LEFT JOIN variants
		ON products.id = variants.product_id
		
		GROUP BY products.id
		ORDER BY id DESC LIMIT ?, ?
		
		");	
		$results->bind_param("dd",  $position, $item_per_page); 
		
		
  	}else{
		$results = $conn->prepare("SELECT 
		products.id, products.product_name, products.cat_id, products.img_array, products.description,
		variants.product_id,
		variants.price_out,
		variants.price_out_sale
		FROM products
		LEFT JOIN variants
		ON products.id = variants.product_id
		WHERE {$tag}=1 
		GROUP BY products.id
		ORDER BY id DESC LIMIT ?, ?");	
		$results->bind_param("dd",  $position, $item_per_page);
    }
}




$results->execute(); //Execute prepared Query
$results->bind_result($pid, $title, $cid, $img_array, $desc, $vid, $p_out, $p_out_sale); //bind variables to prepared statement





//output results from database



$n = 0;
while($results->fetch()){ //fetch values
	
	
	    
	
	
                echo '<div class="box_mobile">';
                //echo $vid.'->'.$p_out.'->'.$p_out_sale;
                //$pics_arr =  explode(",", $img_array);
                
                
                
                ?>

<div class="one_prod">
						<div class="mob_hero" style="background-image: url(<?php echo '../images/products_pics/'.$pid.'/1.jpg';?>);" onclick="prod(<?php echo $pid;?>);"></div>
						<div class="prod_data">
							<div class="inbox bt">
								<div class="prod_title inl_m" onclick="prod(<?php echo $pid;?>);">
									<div class="elips"></div>
									<?php echo $title?>
								</div>
								<div class="prod_price inl_m">
									<div class="inprice" onclick="prod(<?php echo $pid;?>);">
										<?php 
											if($p_out_sale>0){
												$showprice = $p_out_sale;
											}else{
												$showprice = $p_out;
											}
											
											echo number_format($showprice, 0, '.', ' ');
											
										?> <span>грн.</span></div>
								</div>
							</div>
						</div>
						<!-- <div class="inbox bt">
							<div class="share_this">
								<div class="share_title inl_m">Поделиться </div>
								<div class="share_set inl_m">
									<div class="share_icon inl_m fb" onclick="shareOneFB('<?php echo $title;?>','<?php echo $pid;?>','<?php echo 'TEXT';?>');"></div>
									<div class="share_icon inl_m tw" onclick="shareOneTW('<?php echo $title;?>','<?php echo $pid;?>');"></div>
									<div class="share_icon inl_m gp"></div>
									<div class="share_icon inl_m vk" onclick="shareOneVK('<?php echo $title;?>','<?php echo $pid;?>','<?php echo 'TEXT';?>');"></div>
								</div>
							</div>
						</div> -->
					</div>
				</div>


<?php 
}
?>
	

















