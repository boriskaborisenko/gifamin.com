<?php
	header( "content-type: application/xml; charset=UTF-8" );
	$root = 'http://'.$_SERVER['SERVER_NAME'].'/';
	//$root = 'http://test.gifamin.com/';
	include('connect.php');
	
	
	
	$date = date("Y-m-d H:i:s");
	echo '<yml_catalog date="'.$date.'"> ';
	?>
	
	

	

    <shop> 
        <name>Gifamin</name> 
        <company>Gifamin</company> 
        
        <categories> 
		<?php
	       $query = $conn->query("SELECT * FROM p_cats WHERE status=1");
	       while($row = $query->fetch_assoc()){
		       echo '<category id="'.$row['id'].'">'.$row['name'].'</category> ';
	       }
	       
       	?>
	   	</categories> 
        
        
        
        
        
        <offers> 
            
            <?php 
				$query = $conn->query("SELECT * FROM products WHERE start=1");
				while($row = $query->fetch_assoc()){
				
				print_r('
		       <offer id="'.$row['id'].'" productId="'.$row['id'].'" quantity="'.$row['stock'].'"> 
                <url>'.$root.'gift/7387438787428'.$row['id'].'/</url> 
                <price>'.$row['price_out'].'</price> 
                <purchasePrice>'.$row['price_in'].'</purchasePrice> 
                <categoryId>'.$row['cat_id'].'</categoryId> 
                <picture>'.$root.'images/products_pics/'.$row['id'].'/1.jpg</picture> 
                <name>'.htmlspecialchars($row['name']).'</name> 
                <xmlId>'.$row['id'].'</xmlId> 
                <productName>'.htmlspecialchars($row['name']).'</productName> 
                <vendor>Gifamin</vendor> 
                <unit code="pcs" name="Штука" sym="шт." /> 
                <vat_rate>0</vat_rate>
            </offer>');
		         
	       		}
	            
            ?>
            
            
		</offers> 
    </shop> 
</yml_catalog>
