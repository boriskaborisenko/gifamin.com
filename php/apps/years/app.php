<style>
	.myapp{margin-bottom: 180px;}
	.myapp_title{text-align: center; padding-top: 120px; font-size: 24px; font-weight: bold; }
	
	.app_svg{margin-top: 100px;}
	
	.myapp_anno{text-align: center; width: 70%;  margin: 0px auto; color:rgba(255,255,255,0.74); font-size: 13px;}
	.myapp_sep{height: 1px; width: 70%; background:rgba(0,0,0,0.43); margin: 30px auto; opacity: 0.2;}
	#insep_mark{width: 5px; height: 5px; position: absolute; margin-top:-76px;}
	
	#ap_years_res{min-height: 50px; background: transparent;}
	
	.myapp_input{margin: 0 auto; text-align: center; font-weight: bold; margin-bottom: 5px;}
	#app_years_input{text-align: center; outline: none; border-radius: 6px; border:1px solid rgba(0,0,0,0.24);; background: #2c3546; width: 120px; font-size: 60px; padding: 10px 0; color:rgba(0,0,0,0.84); box-shadow: inset 0 0 6px rgba(0,0,0,0.23); color:#ffffff;
		text-shadow: 1px 1px 1px navy;
		 }
	
	
	
	
	#age_error{background: transparent; text-align: center; width: 100%; color:crimson; font-weight: bold; font-size: 11px; position: absolute; color:#EEEEEE;}
	#was_born{text-align: center; font-weight: bold; font-size: 11px; position: absolute; width: 100%; color:rgba(255,255,255,0.7);}
	
	.inp_anno{font-size: 10px; font-weight: bold; text-align: center; color:rgba(0,0,0,0.44);}
	
	.myapp_btn{width: 180px; margin: 30px auto 40px auto;}
	
	
	
	
	.years_all{text-align: center;}
	
	
	
	.magic{position: absolute; height: 180px; width: 100%; margin-top: -220px;}
	.anim_anno{text-align: center; width: 70%;  margin: 40px auto 0 auto; font-weight: bold; color:#229fff;}
	
	.view {
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  -webkit-perspective: 400;
          perspective: 400;
}

.plane {
  width: 120px;
  height: 120px;
  -webkit-transform-style: preserve-3d;
          transform-style: preserve-3d;
}
.plane.main {
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  -webkit-transform: rotateX(60deg) rotateZ(-30deg);
          transform: rotateX(60deg) rotateZ(-30deg);
  -webkit-animation: rotate 20s infinite linear;
          animation: rotate 20s infinite linear;
}
.plane.main .circle {
  width: 120px;
  height: 120px;
  position: absolute;
  -webkit-transform-style: preserve-3d;
          transform-style: preserve-3d;
  border-radius: 100%;
  box-sizing: border-box;
  box-shadow: 0 0 60px #229fff, inset 0 0 60px #229fff;
}
.plane.main .circle::before, .plane.main .circle::after {
  content: "";
  display: block;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  width: 4%;
  height: 4%;
  border-radius: 100%;
  background: #ffffff;
  box-sizing: border-box;
  box-shadow: 0 0 60px 2px #229fff;
}
.plane.main .circle::before {
  -webkit-transform: translateZ(-90px);
          transform: translateZ(-90px);
}
.plane.main .circle::after {
  -webkit-transform: translateZ(90px);
          transform: translateZ(90px);
}
.plane.main .circle:nth-child(1) {
  -webkit-transform: rotateZ(72deg) rotateX(63.435deg);
          transform: rotateZ(72deg) rotateX(63.435deg);
}
.plane.main .circle:nth-child(2) {
  -webkit-transform: rotateZ(144deg) rotateX(63.435deg);
          transform: rotateZ(144deg) rotateX(63.435deg);
}
.plane.main .circle:nth-child(3) {
  -webkit-transform: rotateZ(216deg) rotateX(63.435deg);
          transform: rotateZ(216deg) rotateX(63.435deg);
}
.plane.main .circle:nth-child(4) {
  -webkit-transform: rotateZ(288deg) rotateX(63.435deg);
          transform: rotateZ(288deg) rotateX(63.435deg);
}
.plane.main .circle:nth-child(5) {
  -webkit-transform: rotateZ(360deg) rotateX(63.435deg);
          transform: rotateZ(360deg) rotateX(63.435deg);
}

@-webkit-keyframes rotate {
  0% {
    -webkit-transform: rotateX(0) rotateY(0) rotateZ(0);
            transform: rotateX(0) rotateY(0) rotateZ(0);
  }
  100% {
    -webkit-transform: rotateX(360deg) rotateY(360deg) rotateZ(360deg);
            transform: rotateX(360deg) rotateY(360deg) rotateZ(360deg);
  }
}

@keyframes rotate {
  0% {
    -webkit-transform: rotateX(0) rotateY(0) rotateZ(0);
            transform: rotateX(0) rotateY(0) rotateZ(0);
  }
  100% {
    -webkit-transform: rotateX(360deg) rotateY(360deg) rotateZ(360deg);
            transform: rotateX(360deg) rotateY(360deg) rotateZ(360deg);
  }
}


.one_bottle{width: 120px; min-height: 120px;  margin: 20px 10px; cursor: pointer;}	
.bottle_pic{width: 80px; height: 80px; margin: 0 auto 12px auto; background: #fff url() center center no-repeat; background-size: cover; border-radius: 1000px;  border:10px solid #2c3546;}
.bottle_name{font-weight: bold; color:#fff;}
.bottle_year{}



.res_pop{min-height: 300px; width: 100%; border-top:0px solid #eee; }	


.down_tri{
width: 0;
height: 0;
left:50%;
margin-left: -14px;
position: absolute; z-index: 10;
border-style: solid;
border-width: 14px 15px 0 15px;
border-color: #34405a transparent transparent transparent;
line-height: 0px;
_border-color: #34405a #000000 #000000 #000000;
_filter: progid:DXImageTransform.Microsoft.Chroma(color='#000000');
}


.up_tri{
width: 0;
height: 0;
border-style: solid;
left:50%;
margin-top: -13px; margin-left: -14px;
position: absolute; z-index: 10;
border-width: 0 15px 14px 15px;
border-color: transparent transparent #34405a transparent;
line-height: 0px;
_border-color: #000000 #000000 #34405a #000000;
_filter: progid:DXImageTransform.Microsoft.Chroma(color='#000000');
}

.res_pop_cont{background: #fff; padding: 40px 20px; width:100%; margin-left: 1px; box-sizing: border-box; position: relative; height: 100%; font-size: 0;}
.res_pop_cont_left{width: 52%; padding-right: 20px; box-sizing: border-box;}
.res_pop_cont_right{width: 45%;}
.res_pop_img{position: relative; width: 100%; height: 390px; background:#fff url() center center no-repeat; background-size: contain; border:0px solid #eee; border-radius: 4px;}

.res_pop_title{font-size: 16px; font-weight: bold; text-transform: uppercase;}
.res_pop_year{font-size: 12px; font-weight: bold; margin-top: 6px;}
.res_pop_small_desc{margin: 8px auto 0px auto; font-size: 12px;}
.res_pop_price{font-size: 11px; margin-top: 20px; font-weight: bold;}
.res_pop_price span{font-size: 16px;}
.res_pop_price_sum{font-size: 20px!important;}
.res_pop_price_anno{font-size: 11px; margin-top: 0px;}

.res_pop_fields{margin-top: 20px;}
.res_pop_inp{margin-bottom: 5px;}

.bt1_sky{color:#2D3545; border-color:#229fff;  background: #229fff;}

.bt1_none{color:rgba(0,0,0,0.2); border-color:rgba(0,0,0,0.2);}
.bt_close{color:rgba(0,0,0,0.54);}

.years_button{position: relative;}
.years_hider{position: absolute; background: rgba(255,0,0,0.0); width: 100%; height: 100%;}

.hyper_small{font-size: 11px!important;}

.res_pop_thanks{position: absolute; width: 100%; height: 100%; background:#fafbfc; top:0; box-sizing: border-box; padding: 0px; margin-left: -20px;}
.res_pop_thanks_cont{position: relative; width: 100%; height: 100%; box-sizing: border-box; background: transparent;  /*box-shadow: 0 0 7px rgba(0,0,0,0.23);*/ font-size: 14px; border:0px solid #eee; border-radius: 4px;}

.res_float{position: absolute; text-align: center;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);}
  
  .res_thanks_text{font-size: 20px; font-weight: bold;}
  .res_thanks_anno{margin-top: 8px; font-size: 12px; }
  
  .no_results{font-size: 16px; font-weight: bold; color:#fff;}
  
  
  .close_bottle{  background: url() center center no-repeat; background-size: cover; position: absolute; left: 12px; top:12px; cursor:pointer; border-radius: 1000px; border:1px solid #ebebeb; width: 24px;  height: 24px; text-align:}
  
  .close_bottle span{font-size: 12px!important;}
</style>



<!--APP-->


<div class="myapp">
			<!-- <div class="myapp_title">Years Application</div> -->
			
			<div class="app_svg"><img src="php/apps/years/img/title_8.svg" alt=""></div>
			
	<div class="myapp_anno"> Скажите сколько лет исполняется имениннику,<br> а мы поищем подарок</div>
	<div class="myapp_sep" id="sep"><div id="insep_mark"></div></div>
	
	<div class="myapp_input">
		<input id="app_years_input" type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57'maxlength="3" >
	</div>
	
	<!-- <div class="inp_anno">Возраст</div> -->
	
	<div id="age_error"></div>
	<div id="was_born"></div>
	
	<div class="myapp_btn">
		<div class="btn bt1_sky" onclick="appYearsButton();">Клац!</div>
	</div>
	
	
	
	
	<div id="age_anim" class="myloader off">
		<div class="magic">
			<div class="view">  <div class="plane main">    <div class="circle"></div>    <div class="circle"></div>    <div class="circle"></div>    <div class="circle"></div>    <div class="circle"></div>    <div class="circle"></div>  </div></div>
		</div>
		<div class="anim_anno">Добавляем немного магии...</div>
	</div>
	
	
	<div id="res_pop" class="res_pop off">
		<div class="down_tri"></div>
		<div class="res_pop_cont" id="bottle_data"></div>
		<div class="up_tri"></div>
	</div>
	
	<div id="ap_years_res"></div>
	
	
	
	
</div>