<?php
	header('Content-type: text/xml');
	include('connect.php');
	
	$date = date('Y-m-d H:i:s');
	
	$out = '<?xml version="1.0" encoding="UTF-8"?>
<yml_catalog date="'.$date.'"> 
    <shop> 
     <name>Gifamin</name> 
        <company>Gifamin</company>';
	
	
	$out .= '<categories>';
	
	
	
		$query = $conn->query("SELECT * FROM p_cats WHERE status='1'");
		while($row = $query->fetch_assoc()){
			$out .= '<category id="'.$row['id'].'">'.$row['name'].'</category> ';
		}
	
	
	
	$out .= '</categories>';
	 
    
    
    
    $out .= '<offers>';
    
		$query = $conn->query("SELECT * FROM products");
		while($row = $query->fetch_assoc()){
			$out.= '
			<offer id="'.$row['id'].'" productId="'.$row['id'].'" quantity="'.$row['stock'].'"> 
                <url>http://gifamin.com/gift/7387438787428'.$row['id'].'</url> 
                <price>'.$row['price_out'].'</price> 
                <purchasePrice>'.$row['price_in'].'</purchasePrice> 
                <categoryId>'.$row['cat_id'].'</categoryId> 
                <picture>http://gifamin.com/images/products_pics/'.$row['id'].'/1.jpg</picture> 
                <name>'.$row['name'].'</name> 
                <xmlId>'.$row['id'].'</xmlId> 
                <productName>'.$row['name'].'</productName> 
                <unit code="pcs" name="Штука" sym="шт." /> 
                
            </offer>
			
			';
		}
    
    $out .= '</offers>';        
        
	
	
	
	
	$out.='
      </shop>  
      </yml_catalog>';
      
      
      print($out);
	
?>



