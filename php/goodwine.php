<?php
	header( "content-type: application/xml; charset=UTF-8" );
?>
<?xml version="1.0" encoding="UTF-8"?>
<yml_catalog date="2016-06-20 10:09:18"> 
    <shop> 
        <name>Goodwine-bs</name> 
        <company>Goodwine</company> 
        
        <categories> 
            <category id="1">Вино</category> 
            <category id="2">Виски</category> 
            <category id="3" parentId="2">Однослодовые</category> 
        </categories> 
        
        
        <offers> 
            
            
            <offer id="115" productId="43" quantity="16"> 
                <url>http://link_to_product_page</url> 
                <price>14000.00</price> 
                
                <purchasePrice>13200.00</purchasePrice> 
                
                <categoryId>3</categoryId> 
                <picture>http://link_to_image.jpg</picture> 
                
                <name>Product name</name> 
                <productName>Product name</productName> 
                
                <param name="Артикул" code="sku">789789</param> 
                <param name="Винтаж" code="vintage">1985</param>
                
                
                
                <vendor>Vendor name</vendor> 
                
                <param name="Объем" code="weight">0.7</param>
                <unit code="pcs" name="Штука" sym="шт." /> 
            </offer>
            
            
         </offers> 
    </shop> 
</yml_catalog>   