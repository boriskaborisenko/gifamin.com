<?php
	
	//$to = 'boriskaborisenko@gmail.com';
	//$to = $_GET['to'];
	$to = $um;
	
	$pic_path = 'http://release.gifamin.com/';
	
	$subject = 'Order ID: ('. $new_id.')';
	$headers = "From: <info@gifamin.com>\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
    
    $prev_text = 'Thank you!';
	
	$mail_under_text = ' Undertext';
	
	
	
	$message .= '
	
	<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8"> <!-- utf-8 works for most cases -->
	<meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn"t be necessary -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
	<title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->

	<!-- Web Font / @font-face : BEGIN -->
	<!-- NOTE: If web fonts are not required, lines 10 - 27 can be safely removed. -->

	<!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
	<!--[if mso]>
		<style>
			* {
				font-family: sans-serif !important;
			}
		</style>
	<![endif]-->

	<!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
	<!--[if !mso]><!-->
		<!-- insert web font reference, eg: <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet" type="text/css"> -->
	<!--<![endif]-->

	<!-- Web Font / @font-face : END -->

	<!-- CSS Reset -->
    <style>

		/* What it does: Remove spaces around the email design added by some email clients. */
		/* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
        html,
        body {
	        margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }

        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        /* What is does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin:0 !important;
        }

        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }

        /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }
        table table table {
            table-layout: auto;
        }

        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode:bicubic;
        }

        /* What it does: A work-around for iOS meddling in triggered links. */
        *[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
        }

        /* What it does: A work-around for Gmail meddling in triggered links. */
        .x-gmail-data-detectors,
        .x-gmail-data-detectors *,
        .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
        }

        /* What it does: Prevents Gmail from displaying an download button on large, non-linked images. */
        .a6S {
	        display: none !important;
	        opacity: 0.01 !important;
        }
        /* If the above doesn"t work, add a .g-img class to any image in question. */
        img.g-img + div {
	        display:none !important;
	   	}

        /* What it does: Prevents underlining the button text in Windows 10 */
        .button-link {
            text-decoration: none !important;
        }

        /* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
        /* Create one of these media queries for each additional viewport size you"d like to fix */
        /* Thanks to Eric Lepetit @ericlepetitsf) for help troubleshooting */
        @media only screen and (min-device-width: 375px) and (max-device-width: 413px) { /* iPhone 6 and 6+ */
            .email-container {
                min-width: 375px !important;
            }
        }

    </style>

    <!-- Progressive Enhancements -->
    <style>

        /* What it does: Hover styles for buttons */
        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }
        .button-td:hover,
        .button-a:hover {
            background: #555555 !important;
            border-color: #555555 !important;
        }

        /* Media Queries */
        @media screen and (max-width: 600px) {

            .email-container {
                width: 100% !important;
                margin: auto !important;
            }

            /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
            .fluid {
                max-width: 100% !important;
                height: auto !important;
                margin-left: auto !important;
                margin-right: auto !important;
            }

            /* What it does: Forces table cells into full-width rows. */
            .stack-column,
            .stack-column-center {
                display: block !important;
                width: 100% !important;
                max-width: 100% !important;
                direction: ltr !important;
            }
            /* And center justify these ones. */
            .stack-column-center {
                text-align: center !important;
            }

            /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
            .center-on-narrow {
                text-align: center !important;
                display: block !important;
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }
            table.center-on-narrow {
                display: inline-block !important;
            }

        }

    </style>

</head>
<body width="100%" bgcolor="#ededed" style="margin: 0; mso-line-height-rule: exactly;">
	<center style="width: 100%; background: #ededed; text-align: left;">

        
        
        <!-- Visually Hidden Preheader Text : BEGIN -->
        <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
            
            '.$prev_text.'
            
        </div>
        <!-- Visually Hidden Preheader Text : END -->

        
        
        
        <!-- Email Header : BEGIN -->
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container">
			<tr>
				<td style="padding: 20px 0; text-align: center">
					<img src="'.$pic_path.'/images/mail/maillogo.gif" width="200" height="50" alt="alt_text" border="0" style="height: auto; background: transparent; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;">
				</td>
			</tr>
        </table>
        <!-- Email Header : END -->

        
        
        
        
        <!-- Email Body : BEGIN -->
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container">

            
            
            <!-- Hero Image, Flush : BEGIN -->
            <tr>
            				<td bgcolor="#ffffff">
                        <img src="'.$pic_path.'/images/mail/place.jpg" width="600" height="" alt="alt_text" border="0" align="center" style="width: 100%; max-width: 600px; height: auto; background: #ffffff; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;" class="g-img">
            				</td>
            </tr>
            <!-- Hero Image, Flush : END -->

            
            
            
            
            
            
            
            <!-- 1 Column Text : BEGIN -->
            <!-- <tr>
                <td bgcolor="#ffffff" style="padding: 40px; text-align: center; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;">
                    '.$mail_under_text.'
                    <br><br> -->
                    <!-- Button : Begin -->
                    <!-- <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" style="margin: auto">
                        <tr>
                            <td style="border-radius: 3px; background: #222222; text-align: center;" class="button-td">
                                <a href="http://www.google.com" style="background: #222222; border: 15px solid #222222; font-family: sans-serif; font-size: 13px; line-height: 1.1; text-align: center; text-decoration: none; display: block; border-radius: 3px; font-weight: bold;" class="button-a">
                                    &nbsp;&nbsp;&nbsp;&nbsp;<span style="color:#ffffff;">A Button</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                </a>
                            </td>
                        </tr>
                    </table> -->
                    <!-- Button : END -->
               <!--  </td>
                            </tr> -->
            <!-- 1 Column Text : BEGIN -->

            
            
            
            
            
            
            
            <!-- Background Image with Text : BEGIN -->
            <tr>
                <!-- Bulletproof Background Images c/o https://backgrounds.cm -->
                <td background="" bgcolor="#ffffff" valign="middle" style="text-align: center; background-position: center center !important; background-size: cover !important;">

                    <!--[if gte mso 9]>
                    <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:600px;height:175px; background-position: center center !important;">
                    <v:fill type="tile" src="http://placehold.it/600x230/222222/666666" color="#222222" />
                    <v:textbox inset="0,0,0,0">
                    <![endif]-->
                    <div>
                        <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td valign="middle" style="text-align: left; padding: 40px 28px 0px 28px;  font-family: sans-serif; font-size: 22px; line-height: 16px;  color: #232323;">
                                    Order ID: '.$new_id.'
                                </td>
                            </tr>
                        </table>
                        </div>
                    <!--[if gte mso 9]>
                    </v:textbox>
                    </v:rect>
                    <![endif]-->
                </td>
            </tr>
            <!-- Background Image with Text : END -->

            
            
            
            <tr>
	            <td>
		            <div style="padding:30px 20px; background: #ffffff;">
		            
		            ';
			            
			            
			            
			            
			            
			            
			            
			$message .='            
			            
			            <table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
				            <th style="width: 45%; padding-left: 10px; padding-bottom: 10px; font-family: sans-serif; font-size: 10px; border-bottom: 1px solid #eeeeee; color:#cccccc;">Наименование</th>
				            <!-- <th style="width: 18%; padding-bottom: 10px; text-align: center; font-family: sans-serif; font-size: 10px; border-bottom: 1px solid #eeeeee; color:#cccccc;">Цена</th> -->
				            <!-- <th style="width: 2%; padding-bottom: 10px; text-align: center; font-family: sans-serif; font-size: 10px; border-bottom: 1px solid #eeeeee; color:#cccccc;"></th> -->
				            <th style="width: 14%; padding-bottom: 10px; text-align: center; font-family: sans-serif; font-size: 10px; border-bottom: 1px solid #eeeeee; color:#cccccc;">Кол-во</th>
				            <th style="width: 25%; padding-bottom: 10px; padding-right: 10px; text-align: right; font-family: sans-serif; font-size: 10px; border-bottom: 1px solid #eeeeee; color:#cccccc;">Стоимость</th>';
				            
				          
				          
				          
				          
		 $xx = 0;	 
		 foreach($one['products'] as $product){
		  $xx=$xx+1;
		  $pn =  $product['name'];
		  //$vn =  $product['variant'];
		  $pid = $product['id'];
		  //$vid = $product['variant_id'];
		  $price = $product['price'];
		  $qty = $product['amount'];
		  $tp = $price*$qty;
		  $form_price = number_format($tp, 0, '', ' ');
		  $form_total = number_format($total, 0, '', ' ');
		  
		  $message .= '
		  <tr style="border-bottom: 1px solid #eeeeee; color:#777777; margin-bottom: 8px; padding-bottom: 8px;">
					            <td style="font-family: sans-serif; font-size: 11px; text-align: left; padding:10px 0 10px 10px;">'.$pn.'</td>
					            <td style="font-family: sans-serif; font-size: 11px; text-align: center;  border-right: 1px solid #eeeeee; padding:10px 0 10px 0px;"><span style="font-size: 8px">&#10005;</span> '.$qty.'</td>
					            <td style="font-family: sans-serif; font-size: 11px; text-align: right; padding:10px 10px 10px 0px;">'.$form_price.' грн.</td>
				            </tr>
		  ';
		  }
				          
				          
				          
				          
				          
				            
				            
			$message .= '	            
				            
				            <tr style="border-bottom: 1px solid #eeeeee; color:#777777; margin-bottom: 8px; padding-bottom: 8px;">
					            <td style="font-family: sans-serif; font-size: 11px; text-align: left; padding:10px 0 10px 10px;">'.$dts.'</td>
					            <td style="font-family: sans-serif; font-size: 11px; text-align: center;  border-right: 1px solid #eeeeee; padding:10px 0 10px 0px;"><span style="font-size: 8px"></span> </td>
					            <td style="font-family: sans-serif; font-size: 11px; text-align: right; padding:10px 10px 10px 0px;">'.$dp.' грн.</td>
				            </tr>
				            
				            ';
				            
				            
				$message .= '
				
				<tr style="border-bottom: 0px solid #eeeeee; color:#777777; margin-bottom: 8px; padding-bottom: 8px;">
					            <td style="font-family: sans-serif; font-size: 11px; text-align: left; padding:10px 0 10px 10px;"><b>Общая стоимость: </b></td>
					            <td style="font-family: sans-serif; font-size: 12px; text-align: center;  border-right: 0px solid #eeeeee; padding:10px 0 10px 0px;"><span style="font-size: 8px"></span> </td>
					            <td style="font-family: sans-serif; font-size: 12px; text-align: right; padding:10px 10px 10px 0px;"><b>'.$form_total.' грн.</b></td>
				            </tr>
				
				';            
			            
			            

	


$message .= '</table>
			            </div>
	            </td>
	            
            </tr>
           <!-- 1 Column Text + Button : BEGIN -->
            <tr>
                <td bgcolor="#ffffff" style="padding-top: 10px;">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                    	<tr style="border-top:0px solid #eeeeee; ">
                            <td style="padding: 0px 30px 40px 30px; font-family: sans-serif; font-size: 13px; line-height: 18px; color: #888888;">
                               
                               
                              '.$mail_under_text.'
                               
                            </td>
							</tr>
                    </table>
                </td>
            </tr>
            <!-- 1 Column Text + Button : BEGIN -->

        </table>
        <!-- Email Body : END -->

        <!-- Email Footer : BEGIN -->
        <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="600" style="margin: auto;" class="email-container">
            <tr>
                <td style="padding: 40px 10px;width: 100%;font-size: 12px; font-family: sans-serif; line-height:18px; text-align: center; color: #888888;" class="x-gmail-data-detectors">
                    <webversion style="color:#2D8AD9; text-decoration:none; font-weight: bold;"><a href="http://gifamin.com" style="color:#2D8AD9; text-decoration: none;">www.gifamin.com</a></webversion>
                    <br><br>
                    <!-- (044) 221 28 08<br>(093) 221 28 08<br>Gifamin
                    <br><br> -->
                    <!-- <unsubscribe style="color:#888888; text-decoration:underline;">unsubscribe</unsubscribe> -->
                </td>
            </tr>
        </table>
        <!-- Email Footer : END -->

    </center>
</body>
</html>
	';
	
	
	
	
	mail($to, $subject, $message, $headers);
	//echo "Mail SENT!\r\n";
?>