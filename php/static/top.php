<?php include 'gtag_body.php'?>
<?php include 'ecommerce.php'?>

<div class="all_top">
	<div class="notify off">
		<div class="inner" tkey="beta"></div>
   </div>
	
	<div class="top">
		<div class="inner">
			<div class="top_nav">
				<div class="part_3 inl_m ta_l">
					<div class="open_search animated hide_me" onclick="openSearch();"></div>
					<input type="text" id="mainsearch" class="inp searchinp collapse_search ex" placeholder="" onkeyup="totalSearch(this.value)" autocomplete="off">
					<!-- <div class="go_search inl_m" id="lets_search" onclick="totalSearch(document.getElementById('mainsearch').value)">GO!</div> -->
					<div id="searchres" class="inl_m" style="margin-left: 10px;"></div>
					<div class="close_search inl_m hide_me" onclick="closeSearch();"></div>
				</div>
				<div class="part_3 inl_m ta_c">
					<div class="logo"><a href="/"><img src="images/svg/logo.svg" class="scalepic" alt=""></a></div>
				</div>
				
				<div class="part_3 inl_m ta_r">
					
					<div class="top_menu_right inl_m">
						<ul>
							<!-- <li><a href="/giftcards" tkey="giftcards_top">Giftcards</a></li> -->
							<li><a href="/about" tkey="top_menu_about"></a></li>
							<li><a href="/contacts" tkey="top_menu_contacts"></a></li>
							<!-- <li><a href="/about2.php">About Text</a></li> -->
							<!-- <li><a href="/contacts" tkey="top_menu_contacts">Contact</a></li>
							<li onclick="changeLang('ru');">RU</li>
							<li onclick="changeLang('en');">EN</li>
							<li onclick="changeLang('ua');">UA</li> -->
							
							
						</ul>
					</div>
					
					<div id="langmenu" class="inl_m langmenu">
							
							<div class="inl_m"><a href="javascript:void(0);" tkey="lang_chooser"></a></div>
							<div class="ar_d inl_m"></div>
							 <div class="dropdown-content">
							 	<div class="one_lang" onclick="changeLang('ua');">Українська</div>
							 	<div class="one_lang" onclick="changeLang('ru');">Русский</div>
							 	<!-- <div class="one_lang" onclick="changeLang('en');">English</div> -->
  							 </div>
							</div>
					
					
					
					<div class="for_cart inl_m">
						<a id="unlock_cart" href="javascript:void(0);"><div class="cart inl_m">
							<div class="cartfull off">0</div>
						</div></a>
				    </div>
				
				
				</div>
			</div>
		</div>
	</div>
	
	<!-- 	<div class="top_add bt headroom">
		<div class="inner">
			<div class="breads">
				<ul>
					<li><a href="#">Root</a></li>
					<li><a href="#">Category</a></li>
					<li><a href="#">Subcategory</a></li>
				</ul>
			</div>
		</div>
	</div> -->
	
	
	</div>
	
	