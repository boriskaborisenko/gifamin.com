<?php

include('connect.php');



$item_per_page = 3;

$page_number = filter_var($_GET["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);

//$page_number = filter_var($_GET["page"]);
//$page_number = 1;

if(!is_numeric($page_number)){
    header('HTTP/1.1 500 Invalid page number!');
    exit();
}

$position = (($page_number-1) * $item_per_page);


//$tag = 'start';






	    
	
		
		$results = $conn->prepare("SELECT 
		products.id, products.name, products.description, products.images, products.price_out, products.price_sale
		FROM products
		WHERE visible=1
		AND start=1
		ORDER BY id DESC LIMIT ?, ?
		");	
		$results->bind_param("dd",  $position, $item_per_page); 
		
	
	
	
		    





$results->execute(); //Execute prepared Query
$results->bind_result($pid, $p_name, $p_desc, $p_images, $price_out, $price_sale); //bind variables to prepared statement





//output results from database



$n = 0;
while($results->fetch()){ //fetch values
	
	
	
	 ?>
	 <div class="box">
	  <div class="one_prod">
		  <div class="prod_image bigimg" style="background-image: url(<?php echo '../images/products_pics/'.$pid.'/1.jpg';?>);" onclick="prod(<?php echo $pid;?>);"></div>
		 
		
		 
		 <div class="prod_data">
							<div class="inbox bt">
								<div class="prod_title inl_m" onclick="prod(<?php echo $pid;?>);">
									<div class="elips"></div>
									<?php echo $p_name.' ID: '.$pid?>
								</div>
								<div class="prod_price inl_m">
									<div class="inprice" onclick="prod(<?php echo $pid;?>);">
										<?php 
											if($price_sale>0){
												$showprice = $price_sale;
											}else{
												$showprice = $price_out;
											}
											
											echo number_format($showprice, 0, '.', ' ');
											
										?> <span>грн.</span></div>
								</div>
							</div>
						</div>
						<div class="inbox bt">
							<div class="share_this">
								<!-- <div class="share_title inl_m">Поделиться </div> -->
								<div class="share_set inl_m">
									<div class="share_icon inl_m fb" onclick="shareOneFB('<?php echo $title;?>','<?php echo $pid;?>','<?php echo 'TEXT';?>');"></div>
									<div class="share_icon inl_m tw" onclick="shareOneTW('<?php echo $title;?>','<?php echo $pid;?>');"></div>
									<!-- <div class="share_icon inl_m gp"></div> -->
									<div class="share_icon inl_m vk" onclick="shareOneVK('<?php echo $title;?>','<?php echo $pid;?>','<?php echo 'TEXT';?>');"></div>
								</div>
							</div>
						</div>
		 
	  </div>
	 </div>
	 
	<?php
	}

?>

